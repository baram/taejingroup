import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

export default new VueRouter({
	mode: 'history',
	routes: [
		{
			path: '/',
			redirect: '/view/main',
		},
		{
			//메인
			path: '/view/main',
			component: () => import('../views/MainPage.vue'),
		},
		{
			//컨텐츠
			path: '/view/content/:page',
			component: () => import('../views/ContentPage.vue'),
		},
		{
			//상세
			path: '/view/detail/:page/:idx',
			component: () => import('../views/DetailPage.vue'),
		},
		// {
		// 	path: '/view/aboutCeo',
		// 	component: () => import('../components/about/AboutCeo.vue'),
		// },
	],
});
