import axios from 'axios';
import { setInterceptors } from './common/interceptors';

//인스턴스 만들기
function createInstance() {
	const instance = axios.create({
		baseURL: process.env.VUE_APP_API_URL,
	});
	return setInterceptors(instance);
}

const instance = createInstance();

//토큰 값 넘기기
const headers = {
	'Access-Control-Allow-Origin': '*',
	'Content-type': 'application/json',
	Accept: '*/*',
};

function RecruitList(page) {
	return instance.get(
		'api/contact/recruits' + '?page=' + page.page + '&size=' + page.size,
		{
			headers,
		},
	);
}

function NewstList(page) {
	return instance.get(
		'api/front-news/lists' +
			'?page=' +
			page.page +
			'&searchCondition=' +
			page.searchCondition +
			'&searchKeyword=' +
			page.searchKeyword +
			'&size=' +
			page.size,
		{
			headers,
		},
	);
}

function FetchNews(newsSeq) {
	return instance.get('api/front-news/' + newsSeq);
}

function FetchBusiness(businessSeq) {
	return instance.get('api/business/' + businessSeq);
}

export { RecruitList, NewstList, FetchNews, FetchBusiness };
