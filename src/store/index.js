import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);

import { RecruitList, NewstList, FetchBusiness } from '../api/index.js';

export default new Vuex.Store({
	state: {
		recruit: {},
		news: {},
		business: {},
	},
	mutations: {
		SET_RECRUIT(state, recruit) {
			state.recruit = recruit;
		},
		SET_NEWS(state, news) {
			state.news = news;
		},
		SET_BUSINESS(state, business) {
			state.business = business;
		},
	},
	actions: {
		async LIST_RECRUIT({ commit }, page) {
			const { data } = await RecruitList(page);
			commit('SET_RECRUIT', data);
		},
		async LIST_NEWS({ commit }, page) {
			const { data } = await NewstList(page);
			commit('SET_NEWS', data);
		},
		async LIST_BUSINESS({ commit }, page) {
			const { data } = await FetchBusiness(page);
			commit('SET_BUSINESS', data);
		},
	},
});
