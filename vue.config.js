module.exports = {
	devServer: {
		overlay: false,
		compress: true,
		disableHostCheck: true,
		port: 8080,
	},
	// publicPath: '/view/',
	outputDir: './view',
};
